<section class="px-5 pb-20 md:px-0">
    <div data-aos="fade-up" data-aos-offset="200" data-aos-delay="50" data-aos-duration="900" data-aos-easing="ease-in-out"
        class="container max-w-6xl mx-auto">
        <div x-data="{ selected: 1 }">
            <ul class="shadow-box">

                <li class="relative border-[#00D7C7] border-y-2">

                    <button type="button" class="w-full py-6 text-left text-white"
                        @click="selected !== 1 ? selected = 1 : selected = null">
                        <div class="flex items-center justify-between">
                            <p class="text-xl">
                                What is retainer?</p>
                            <div class="px-6 py-2 text-center bg-gray-700 rounded-full">
                                <p class="text-base">1</p>
                            </div>

                        </div>
                    </button>

                    <div class="relative overflow-hidden text-white transition-all duration-700 max-h-0"
                        x-ref="container1"
                        x-bind:style="selected == 1 ? 'max-height: ' + $refs.container1.scrollHeight + 'px' : ''">
                        <div class="pb-6">
                            <p class="text-base text-[#9a9a9a]">Lorem ipsum dolor sit, amet consectetur adipisicing
                                elit.
                                Sit eum nostrum quasi deleniti
                                vitae. At, animi accusamus. Assumenda laborum optio voluptas unde, ad quo suscipit quae.
                                Beatae corporis consequatur nostrum.</p>
                        </div>
                    </div>

                </li>


                <li class="relative border-b-2 border-[#00D7C7]">

                    <button type="button" class="w-full py-6 text-left text-white"
                        @click="selected !== 2 ? selected = 2 : selected = null">
                        <div class="flex items-center justify-between">
                            <p class="text-xl">
                                How do I subscribe to monthly retainer?</p>
                            <div class="px-6 py-2 text-center bg-gray-700 rounded-full">
                                <p class="text-base">2</p>
                            </div>

                        </div>
                    </button>

                    <div class="relative overflow-hidden transition-all duration-700 max-h-0" style=""
                        x-ref="container2"
                        x-bind:style="selected == 2 ? 'max-height: ' + $refs.container2.scrollHeight + 'px' : ''">
                        <div class="pb-6">
                            <p class="text-base text-[#9a9a9a]">Lorem ipsum dolor sit, amet consectetur adipisicing
                                elit.
                                Sit eum nostrum quasi deleniti
                                vitae. At, animi accusamus. Assumenda laborum optio voluptas unde, ad quo suscipit quae.
                                Beatae corporis consequatur nostrum.</p>
                        </div>
                    </div>

                </li>

                <li class="relative border-b-2 border-[#00D7C7]">

                    <button type="button" class="w-full py-6 text-left text-white"
                        @click="selected !== 3 ? selected = 3 : selected = null">
                        <div class="flex items-center justify-between">
                            <p class="text-xl">
                                How to contact support?</p>
                            <div class="px-6 py-2 text-center bg-gray-700 rounded-full">
                                <p class="text-base">3</p>
                            </div>

                        </div>
                    </button>

                    <div class="relative overflow-hidden transition-all duration-700 max-h-0" style=""
                        x-ref="container3"
                        x-bind:style="selected == 3 ? 'max-height: ' + $refs.container3.scrollHeight + 'px' : ''">
                        <div class="pb-6">
                            <p class="text-base text-[#9a9a9a]">Lorem ipsum dolor sit, amet consectetur adipisicing
                                elit.
                                Sit eum nostrum quasi deleniti
                                vitae. At, animi accusamus. Assumenda laborum optio voluptas unde, ad quo suscipit quae.
                                Beatae corporis consequatur nostrum.</p>
                        </div>
                    </div>

                </li>
            </ul>
        </div>
    </div>
</section>
