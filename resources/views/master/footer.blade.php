<hr class="px-20">
<footer class="bg-white px-5 md:px-0 py-10 pt-24 text-[#14233F]">
    <div class="container max-w-6xl mx-auto">

        <div class="flex flex-col md:flex-row justify-between space-y-24 md:space-y-0">
            <img src="{{ url('assets/images/logo-green.png') }}" alt=""
                class="mx-auto w-2/4 md:mx-0 md:w-auto md:h-8">

            <div class="space-y-12">
                <p class="text-[20px] uppercase font-bold">Development</p>
                <div class="flex flex-col space-y-5 text-[18px]">
                    <a href="#" class="hover:text-[#00D7C7]">Laravel Development</a>
                    <a href="#" class="hover:text-[#00D7C7]">WordPress Development</a>
                    <a href="#" class="hover:text-[#00D7C7]">PHP Web Development</a>
                    <a href="#" class="hover:text-[#00D7C7]">Custom Development</a>
                    <a href="#" class="hover:text-[#00D7C7]">App Development</a>
                </div>
            </div>
            <div class="space-y-12">
                <p class="text-[20px] uppercase font-bold">Strategy & Design</p>
                <div class="flex flex-col space-y-5 text-[18px]">
                    <a href="#" class="hover:text-[#00D7C7]">Strategy</a>
                    <a href="#" class="hover:text-[#00D7C7]">Digital</a>
                    <a href="#" class="hover:text-[#00D7C7]">CX Design</a>
                    <a href="#" class="hover:text-[#00D7C7]">Strategy Workshops</a>
                </div>
            </div>
            <div class="space-y-12">
                <p class="text-[20px] uppercase font-bold">Resource</p>
                <div class="flex flex-col space-y-5 text-[18px]">
                    <a href="#" class="hover:text-[#00D7C7]">Build Your MVP</a>
                    <a href="#" class="hover:text-[#00D7C7]">Support Maintenance</a>
                    <a href="#" class="hover:text-[#00D7C7]">Retainers</a>
                    <a href="#" class="hover:text-[#00D7C7]">Insights</a>
                    <a href="#" class="hover:text-[#00D7C7]">Partners</a>
                </div>
            </div>
        </div>

    </div>
    <div class="container max-w-6xl mx-auto mt-20">
        <div class="flex flex-col md:flex-row space-y-5 md:space-y-0 md:space-x-12">
            <p>Copyright © 2023 TOAT. All Rights Reserved.</p>
            <a href="#" class="hover:text-[#00D7C7]">Privacy</a>
            <a href="#" class="hover:text-[#00D7C7]">Terms</a>
            <a href="#" class="hover:text-[#00D7C7]">Cookies</a>
        </div>
    </div>
</footer>
