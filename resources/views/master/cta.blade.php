<section class="px-5 py-16 mt-16 bg-white md:px-0 md:py-20 md:mt-32">
    <div class="container max-w-6xl mx-auto">
        <div class="grid grid-cols-1 gap-3 md:grid-cols-2">
            <div>
                <div class="flex flex-col">
                    <p data-aos="fade-right" data-aos-offset="200" data-aos-delay="20" data-aos-duration="1000"
                        data-aos-easing="ease-in-out"
                        class="text-[#0A162C] uppercase italic font-black max-w-2xl text-4xl text-center md:text-[50px] md:text-left">
                        Got a Project?
                    </p>
                    <div data-aos="fade-right" data-aos-offset="200" data-aos-delay="20" data-aos-duration="1100"
                        data-aos-easing="ease-in-out" class="mt-10 space-y-3">
                        <p
                            class="text-[#0A162C]  max-w-2xl text-2xl font-normal text-center md:text-[25px] md:text-left">
                            What can we build for you?
                        </p>
                        <p
                            class="text-[#0A162C]  max-w-2xl text-2xl font-normal text-center md:text-[25px] md:text-left">
                            Lets discuss your project...
                        </p>
                    </div>
                    <div data-aos="fade-right" data-aos-offset="200" data-aos-delay="20" data-aos-duration="1150"
                        data-aos-easing="ease-in-out" class="mt-20 space-y-3 md:mt-40">
                        <p
                            class="text-[#0A162C]  max-w-2xl text-2xl font-semibold text-center md:text-[22px] md:text-left">
                            Email
                        </p>
                        <p class="text-[#0A162C]  max-w-2xl text-2xl text-center md:text-[22px] md:text-left">
                            team@toat.agency
                        </p>
                    </div>
                    <div
                        class="flex justify-center md:justify-start text-4xl md:text-[35px] text-[#00D7C7] space-x-3 mt-10">
                        <a data-aos="fade-up" data-aos-offset="200" data-aos-delay="20" data-aos-duration="1100"
                            data-aos-easing="ease-in-out" href="#"><i
                                class="fa-brands fa-linkedin hover:text-[#0A162C]"></i></a>
                        <a data-aos="fade-up" data-aos-offset="200" data-aos-delay="20" data-aos-duration="1150"
                            data-aos-easing="ease-in-out" href="#"><i
                                class="fa-brands fa-square-instagram hover:text-[#0A162C]"></i></a>
                    </div>
                </div>
            </div>
            <div class="" data-aos="fade-up" data-aos-offset="200" data-aos-delay="20" data-aos-duration="1100"
                data-aos-easing="ease-in-out">

                <form method="POST" action="{{ route('contact.store') }}">
                    @csrf
                    <div class="space-y-5">
                        @if (Session::has('success'))
                            <div class="px-6 py-5 mb-3 text-base text-green-700 bg-green-100 rounded-lg" role="alert">
                                {{ Session::get('success') }}
                            </div>
                        @endif
                        <div>
                            <label for="name" class="block mb-2 text-base font-medium text-gray-900">Name <span
                                    class="text-red-500">*</span></label>
                            <input type="text" id="name" name="name"
                                class="block w-full p-5 text-sm text-gray-900 placeholder-gray-400 bg-white border border-gray-300 rounded-lg "
                                placeholder="Write your full name">
                        </div>
                        <div>
                            <label for="email" class="block mb-2 text-base font-medium text-gray-900">Email <span
                                    class="text-red-500">*</span></label>
                            <input type="email" id="email" name="email"
                                class="block w-full p-5 text-sm text-gray-900 placeholder-gray-400 bg-white border border-gray-300 rounded-lg "
                                placeholder="Write your email">
                        </div>
                        <div>
                            <label for="company" class="block mb-2 text-base font-medium text-gray-900">Company <span
                                    class="text-red-500">*</span></label>
                            <input type="text" id="company" name="company"
                                class="block w-full p-5 text-sm text-gray-900 placeholder-gray-400 bg-white border border-gray-300 rounded-lg "
                                placeholder="Write your name name">
                        </div>
                        <div>
                            <label for="project" class="block mb-2 text-base font-medium text-gray-900">Tell us about
                                your project <span class="text-red-500">*</span></label>

                            <textarea id="project" rows="10" name="project"
                                class="block w-full p-5 text-sm text-gray-900 placeholder-gray-400 bg-white border border-gray-300 rounded-lg"
                                placeholder="Write on how we can start your project"></textarea>
                        </div>
                        <div class="flex flex-col space-y-5 md:flex-row md:space-y-0 md:space-x-5">
                            <div class="flex-1">
                                <label for="budget" class="block mb-2 text-base font-medium text-gray-900">Budget
                                    <span class="text-red-500">*</span></label>
                                <input type="number" id="budget" name="budget"
                                    class="block w-full p-5 text-sm text-gray-900 placeholder-gray-400 bg-white border border-gray-300 rounded-lg "
                                    placeholder="Write initial budget">
                            </div>
                            <div class="flex-col flex-1 space-y-3">
                                <div>
                                    <label for="timescale" class="block mb-2 text-base font-medium text-gray-900">Start
                                        Date
                                        <span class="text-red-500">*</span></label>
                                    <input type="date" id="timescale" name="start_date"
                                        class="block w-full p-5 text-sm text-gray-900 placeholder-gray-400 bg-white border border-gray-300 rounded-lg "
                                        placeholder="Write your time scale">
                                </div>
                                <div>
                                    <label for="timescale" class="block mb-2 text-base font-medium text-gray-900">End
                                        Date
                                        <span class="text-red-500">*</span></label>
                                    <input type="date" id="timescale" name="end_date"
                                        class="block w-full p-5 text-sm text-gray-900 placeholder-gray-400 bg-white border border-gray-300 rounded-lg "
                                        placeholder="Write your time scale">
                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="flex items-center justify-start mt-16 space-x-5">
                        <img src="{{ url('assets/content/button-arrow.svg') }}" alt="" />
                        <button type="submit" class="text-[#0A162C] font-semibold text-lg">Submit Enquiry</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</section>
