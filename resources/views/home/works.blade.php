<section class="px-5 md:px-0 py-10">
    <div class="container max-w-6xl mx-auto">
        <p class="text-start text-white text-3xl md:text-[40px] font-normal">Check our Work</p>
    </div>


    <div class="container max-w-6xl mx-auto mt-5">
        <div class="owl-carousel">
            @foreach ($data as $item)
                <a href="{{ env('NOTION_PUBLIC_URI') . $item['id'][0] }}" class="item">
                    <div class="img-overlay relative">
                        <div class="w-full md:p-0">
                            <img src="{{ $item['image'][0] }}" alt="" />
                        </div>
                        <div class="overlay p-10 absolute top-0 bottom-0 right-0 left-0 opacity-0">
                            <div class="flex flex-row items-end h-full">
                                <div class="flex flex-col space-y-3">
                                    <i class="fa-brands fa-laravel text-white text-4xl"></i>
                                    <p
                                        class="italic uppercase text-base font-black  md:text-[28px] text-left text-white">
                                        {{ $item['title'][0] }}</p>
                                </div>
                            </div>
                        </div>

                    </div>
            @endforeach

            </a>
        </div>
    </div>


</section>
