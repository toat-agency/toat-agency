<section class="px-5 md:px-0 py-10 mt-10">
    <div class="container max-w-6xl flex flex-col-reverse md:flex-row items-center justify-between mx-auto">
        <div class="space-y-8">
            <div class="flex font-bold items-center justify-center md:justify-start">
                <p data-aos="fade-right" data-aos-offset="200" data-aos-delay="20" data-aos-duration="600"
                    data-aos-easing="ease-in-out"
                    class="uppercase italic content-number text-6xl md:text-[100px] font-black">01/</p>
                <p data-aos="fade-left" data-aos-offset="200" data-aos-delay="20" data-aos-duration="600"
                    data-aos-easing="ease-in-out"
                    class="uppercase italic text-white text-lg md:text-[30px] font-black -ml-10">Heading Here
                </p>
            </div>
            <p data-aos="fade-up" data-aos-offset="200" data-aos-delay="20" data-aos-duration="600"
                data-aos-easing="ease-in-out"
                class="text-white text-sm text-center font-light leading-[25px] md:text-[20px] md:text-left md:ml-20">We
                deliver long
                term, high
                value
                growth
                for our clients with UX Strategy, latest UI best practice,
                combined with Shopify Plus Development and best-in-class technology partner solutions, to create awesome
                D2C eCommerce websites for our lifestyle, sustainable + health and well-being brands.</p>
            <div data-aos="fade-up" data-aos-offset="200" data-aos-delay="20" data-aos-duration="650"
                data-aos-easing="ease-in-out"
                class="flex items-center space-x-5 justify-center md:justify-start md:ml-20">
                <img src="{{ url('assets/content/button-arrow.svg') }}" alt="" class="w-8 md:w-auto" />
                <a href="#" class="text-white hover:text-[#00D7C7] animate-bounce text-base md:text-[20px]">View
                    Case Study</a>
            </div>
        </div>

        <img src="{{ url('assets/content/case-study-1.png') }}" alt="" class="md:ml-8 minimal-floating" />
    </div>

    <div class="container max-w-6xl grid grid-cols-1 md:grid-cols-3 gap-20 mx-auto py-10 mt-10">
        <div data-aos="fade-up" data-aos-offset="200" data-aos-delay="20" data-aos-duration="1000"
            data-aos-easing="ease-in-out" class="card space-y-[15px]">
            <div class="flex justify-center md:justify-start">
                <img src="{{ url('assets/content/laravel.svg') }}" alt="" />
            </div>

            <p class="text-white uppercase italic text-2xl md:text-[30px] font-black text-center md:text-left">Build
                Your
                MVP</p>
            <p class="text-white text-sm text-center font-light md:text-[20px] md:text-left leading-[25px]">We deliver
                long term,
                high value
                growth
                for our clients with UX Strategy, latest UI best practice, combined with Shopify Plus Development.</p>
        </div>
        <div data-aos="fade-up" data-aos-offset="200" data-aos-delay="20" data-aos-duration="1050"
            data-aos-easing="ease-in-out" class="card space-y-[15px]">
            <div class="flex justify-center md:justify-start">
                <img src="{{ url('assets/content/laravel.svg') }}" alt="" />
            </div>

            <p class="text-white uppercase italic text-2xl md:text-[30px] font-black text-center md:text-left">Build
                Your MVP</p>
            <p class="text-white text-sm text-center font-light md:text-[20px] md:text-left leading-[25px]">We deliver
                long term,
                high value
                growth
                for our clients with UX Strategy, latest UI best practice, combined with Shopify Plus Development.</p>
        </div>
        <div data-aos="fade-up" data-aos-offset="200" data-aos-delay="20" data-aos-duration="1100"
            data-aos-easing="ease-in-out" class="card space-y-[15px]">
            <div class="flex justify-center md:justify-start">
                <img src="{{ url('assets/content/laravel.svg') }}" alt="" />
            </div>

            <p class="text-white uppercase italic text-2xl md:text-[30px] font-black text-center md:text-left">Build
                Your MVP</p>
            <p class="text-white text-sm text-center font-light md:text-[20px] md:text-left leading-[25px]">We deliver
                long term,
                high value
                growth
                for our clients with UX Strategy, latest UI best practice, combined with Shopify Plus Development.</p>
        </div>

    </div>
    <div class="container max-w-6xl flex flex-col md:flex-row items-center justify-between mx-auto mt-10">
        <img src="{{ url('assets/content/case-study-2.png') }}" alt="" class="md:ml-8 minimal-floating" />
        <div class="space-y-8">
            <div class="flex font-bold items-center justify-center md:justify-start">
                <p data-aos="fade-right" data-aos-offset="200" data-aos-delay="20" data-aos-duration="600"
                    data-aos-easing="ease-in-out"
                    class="uppercase italic content-number text-6xl md:text-[100px] font-black">02/</p>
                <p data-aos="fade-left" data-aos-offset="200" data-aos-delay="20" data-aos-duration="600"
                    data-aos-easing="ease-in-out"
                    class="uppercase italic text-white text-lg md:text-[30px] font-black -ml-10">Heading Here
                </p>
            </div>
            <p data-aos="fade-up" data-aos-offset="200" data-aos-delay="20" data-aos-duration="600"
                data-aos-easing="ease-in-out"
                class="text-white text-sm text-center font-light leading-[25px] md:text-[20px] md:text-left md:ml-20">We
                deliver long
                term, high
                value
                growth
                for our clients with UX Strategy, latest UI best practice,
                combined with Shopify Plus Development and best-in-class technology partner solutions, to create awesome
                D2C eCommerce websites for our lifestyle, sustainable + health and well-being brands.</p>
            <div data-aos="fade-up" data-aos-offset="200" data-aos-delay="20" data-aos-duration="650"
                data-aos-easing="ease-in-out"
                class="flex items-center space-x-5 justify-center md:justify-start md:ml-20">
                <img src="{{ url('assets/content/button-arrow.svg') }}" alt="" class="w-8 md:w-auto" />
                <a href="#" class="text-white hover:text-[#00D7C7] animate-bounce text-base md:text-[20px]">View
                    Case Study</a>
            </div>
        </div>
    </div>
</section>
