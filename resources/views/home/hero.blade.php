<section class="px-5 md:px-0 py-20 mt-10 md:mt-20">

    <div
        class="container max-w-6xl flex flex-col-reverse md:flex-row items-center justify-between mx-auto space-y-0 md:space-y-0">

        <div class="flex flex-col space-y-6 md:space-y-[30px]">
            <h1 data-aos="fade-up" data-aos-offset="200" data-aos-delay="50" data-aos-duration="600"
                data-aos-easing="ease-in-out"
                class="text-white max-w-3xl text-2xl text-center font-normal md:text-[35px] md:text-left leading-[35px] md:leading-[45px]">
                <span class="font-bold text-[#00D7C7]">PHP</span>, <span
                    class="font-bold text-[#00D7C7]">WordPress</span>
                & <span class="font-bold text-[#00D7C7] reveal-text">Custom
                    Dev solutions</span>, backed by user
                centric Strategy & UX, to create engaging +
                converting platforms
            </h1>
            <p data-aos="fade-up" data-aos-offset="200" data-aos-delay="50" data-aos-duration="700"
                data-aos-easing="ease-in-out"
                class="text-[#9A9A9A] max-w-2xl text-base font-normal text-center md:text-[20px] md:text-left leading-[25px]">
                We’re a growth-focused <span class="font-bold text-[#00D7C7]">Laravel Development</span> agency,
                partnering with ambitious teams, to help grow and
                scale their digital presence by developing tools, systems + websites.
            </p>
            <div class="flex space-x-5 text-[#9A9A9A] justify-center md:text-[20px] md:justify-start">
                <a href="#" class="hover:text-[#00D7C7]" data-aos="fade-right" data-aos-offset="200"
                    data-aos-delay="50" data-aos-duration="900" data-aos-easing="ease-in-out">Case
                    Studies</a>
                <a href="#" class="hover:text-[#00D7C7]" data-aos="fade-right" data-aos-offset="200"
                    data-aos-delay="50" data-aos-duration="700" data-aos-easing="ease-in-out">Contact</a>
            </div>
        </div>
        <div class="md:w-1/2 p-6" data-aos="zoom-in-up" data-aos-offset="200" data-aos-delay="50"
            data-aos-duration="1000" data-aos-easing="ease-in-out">
            <img src="{{ url('assets/images/hero-image.png') }}" alt="" />
        </div>
    </div>
    <div data-aos="fade-right" data-aos-offset="200" data-aos-delay="50" data-aos-duration="800"
        data-aos-easing="ease-in-out" class="md:w-1/2 mx-auto py-10">
        <img src="{{ url('assets/images/lines.svg') }}" alt="" />
    </div>

</section>
