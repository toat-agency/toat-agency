<section class="px-5 pt-20 md:px-0">
    <div class="container flex flex-row justify-between max-w-6xl mx-auto space-x-12">
        <div data-aos="fade-right" data-aos-offset="200" data-aos-delay="50" data-aos-duration="700"
            data-aos-easing="ease-in-out" class="w-1/4 space-y-5">
            <p class="text-[#0A162C] text-lg">The cornerstone for who we are.</p>
            <p class="text-[#0A162C] text-lg">Why we do what we do.</p>
        </div>
        <div data-aos="fade-right" data-aos-offset="200" data-aos-delay="50" data-aos-duration="900"
            data-aos-easing="ease-in-out" class="w-3/4 space-y-12 text-4xl font-bold">
            <p class="text-[#0A162C]">Our <span class="highlight">love and
                    enthusiasm</span>
                are
                what
                drive our
                work.</p>
            <p class="text-[#0A162C]">We think that the secret to growth is <span class="highlight">
                    <change class="span">change.</change>
            </p>
            <p class="text-[#0A162C]"><span class="highlight">Every
                    time,</span> we manage to
                change the
                No
                into a Yes.</p>
        </div>
    </div>
</section>
