<section class="px-5 py-10 mt-10 md:px-0">
    <p data-aos="fade-up" data-aos-offset="200" data-aos-delay="20" data-aos-duration="500" data-aos-easing="ease-in-out"
        class="text-center text-white text-3xl md:text-[40px] mb-16">Client Roster</p>
    <div class="container grid max-w-6xl grid-cols-3 gap-8 mx-auto md:grid-cols-5">
        <div data-aos="fade-right" data-aos-offset="200" data-aos-delay="20" data-aos-duration="1000"
            data-aos-easing="ease-in-out" class="card">
            <img src="{{ url('assets/awards/uk-dev-awards-2022-finalist.png') }}" alt="" />
        </div>
        <div data-aos="fade-right" data-aos-offset="200" data-aos-delay="20" data-aos-duration="1050"
            data-aos-easing="ease-in-out" class="card">
            <img src="{{ url('assets/awards/uk-dev-awards-2022-winner.png') }}" alt="" />
        </div>
        <div data-aos="fade-right" data-aos-offset="200" data-aos-delay="20" data-aos-duration="1100"
            data-aos-easing="ease-in-out" class="card">
            <img src="{{ url('assets/awards/eu-ecommerce-award-2022.png') }}" alt="" />
        </div>
        <div data-aos="fade-right" data-aos-offset="200" data-aos-delay="20" data-aos-duration="1150"
            data-aos-easing="ease-in-out" class="card">
            <img src="{{ url('assets/awards/uk-ecommerce-award-2021.png') }}" alt="" />
        </div>
        <div data-aos="fade-right" data-aos-offset="200" data-aos-delay="20" data-aos-duration="1200"
            data-aos-easing="ease-in-out" class="card">
            <img src="{{ url('assets/awards/uk-agency-award-2021.png') }}" alt="" />
        </div>
        <div data-aos="fade-right" data-aos-offset="200" data-aos-delay="20" data-aos-duration="1250"
            data-aos-easing="ease-in-out" class="card">
            <img src="{{ url('assets/awards/uk-dev-awards-2022-finalist.png') }}" alt="" />
        </div>
        <div data-aos="fade-right" data-aos-offset="200" data-aos-delay="20" data-aos-duration="1300"
            data-aos-easing="ease-in-out" class="card">
            <img src="{{ url('assets/awards/uk-dev-awards-2022-winner.png') }}" alt="" />
        </div>
        <div data-aos="fade-right" data-aos-offset="200" data-aos-delay="20" data-aos-duration="1350"
            data-aos-easing="ease-in-out" class="card">
            <img src="{{ url('assets/awards/eu-ecommerce-award-2022.png') }}" alt="" />
        </div>
        <div data-aos="fade-right" data-aos-offset="200" data-aos-delay="20" data-aos-duration="1400"
            data-aos-easing="ease-in-out" class="card">
            <img src="{{ url('assets/awards/uk-ecommerce-award-2021.png') }}" alt="" />
        </div>
        <div data-aos="fade-right" data-aos-offset="200" data-aos-delay="20" data-aos-duration="1450"
            data-aos-easing="ease-in-out" class="card">
            <img src="{{ url('assets/awards/uk-agency-award-2021.png') }}" alt="" />
        </div>
    </div>

</section>
