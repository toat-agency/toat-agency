<section class="px-5 py-40 bg-white md:px-0">
    <div class="container flex flex-col max-w-6xl mx-auto space-y-20">
        <div data-aos="fade-up" data-aos-offset="200" data-aos-delay="50" data-aos-duration="900"
            data-aos-easing="ease-in-out"
            class="flex flex-col items-center justify-between space-x-0 space-y-8 md:flex-row md:space-x-12 md:space-y-0">
            <div class="space-y-8">
                <p class="text-[#0A162C] italic uppercase font-bold text-4xl">Strategy Workshops</p>
                <p class="text-[#0A162C] text-xl">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                    eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                    exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
            </div>
            <img src="{{ asset('assets/images/strategy_workshops.jpeg') }}" alt="" class="w-2/5">
        </div>
        <div data-aos="fade-up" data-aos-offset="200" data-aos-delay="50" data-aos-duration="1000"
            data-aos-easing="ease-in-out"
            class="flex flex-col items-center justify-between space-x-0 space-y-8 md:flex-row md:space-x-12 md:space-y-0">
            <img src="{{ asset('assets/images/cx_design.jpeg') }}" alt="" class="w-2/5">
            <div class="space-y-8">
                <p class="text-[#0A162C] italic uppercase font-bold text-4xl">CX Design</p>
                <p class="text-[#0A162C] text-xl">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                    eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                    exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
            </div>
        </div>
    </div>
</section>
