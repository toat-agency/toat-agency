## About TOAT Agency

We’re a growth-focused Laravel Development agency, partnering with ambitious teams, to help grow and scale their digital presence by developing tools, systems + websites.

## How to setup in local

Documentation: [Toat Agency Documentation with Notion API Integration](https://bumpy-zucchini-f00.notion.site/Toat-Agency-Documentation-with-Notion-API-Integration-cdafca7770fe4742a3b1e39334966b1e)
