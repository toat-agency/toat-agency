<section class="px-5 py-20 pt-40 mt-10 mb-40 md:px-0 md:mt-40">

    <div class="container relative flex flex-col items-center justify-between max-w-6xl mx-auto space-y-8 text-center">
        <h1 data-aos="fade-up" data-aos-offset="200" data-aos-delay="50" data-aos-duration="900"
            data-aos-easing="ease-in-out" class="z-10 text-6xl italic font-black text-white uppercase">Monthly <span
                class="underline decoration-[#00D7C7]"> Retainers</span>
        </h1>
        <p data-aos="fade-up" data-aos-offset="200" data-aos-delay="50" data-aos-duration="1000"
            data-aos-easing="ease-in-out" class="text-[#9A9A9A] text-2xl z-10">Lorem ipsum dolor sit amet <span
                class="text-[#00D7C7] font-bold">consectetur</span>
            adipisicing elit. Recusandae, modi
            adipisci. Recusandae minima
            perspiciatis voluptatibus non reprehenderit <span class="text-[#00D7C7] font-bold">molestiae</span> hic
            quidem eos ab cupiditate.</p>
        <div class="absolute w-4/5 transform -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2">
            <img src="{{ asset('assets/images/retainer.png') }}" alt="">
        </div>
    </div>

</section>
