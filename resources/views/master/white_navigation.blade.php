<nav class="fixed top-0 left-0 z-50 w-full px-5 bg-white md:px-0">

    <div class="container flex flex-wrap items-center justify-between max-w-6xl py-8 mx-auto">
        <a href="{{ route('index') }}" id="dark_logo" class="nav-logo"><img src="{{ url('assets/images/dark_logo.png') }}"
                alt="TOAT Agency Logo" /></a>
        <a href="{{ route('index') }}" id="white_logo" class="hidden nav-logo"><img
                src="{{ url('assets/images/logo.png') }}" alt="TOAT Agency Logo" /></a>

        <button id="menu-btn" class="block hamburger focus:outline-none">
            <span class="hamburger-top-dark"></span>
            <span class="hamburger-middle-dark"></span>
            <span class="hamburger-bottom-dark"></span>
        </button>
        <div class="navbar relative left-[-110%]" id="navmenu">
            <div class="nav-items">
                <a href="{{ route('services.index') }}" data-text="01/ Services" data-service="services"><span
                        class="text-[#00D7C7] nav-item-number">01/</span>
                    Services</a>
                <a href="{{ route('retainer.index') }}" data-text="02/ Retainers" data-service="retainers"><span
                        class="text-[#00D7C7] nav-item-number">02/</span>
                    Retainers</a>
                <div class="flex flex-row space-x-3">
                    <a href="{{ route('case-studies.index') }}" data-text="03/ Case
                Studies"
                        data-service="case-studies"><span class="text-[#00D7C7] nav-item-number">03/</span> Case
                        Studies </a>
                    <p class="text-[#00D7C7] nav-item-case text-2xl font-bold">x{{ count($total) }}</p>
                </div>

                <a href="{{ route('partner.index') }}" data-text="04/ Partners" data-service="retainers"><span
                        class="text-[#00D7C7] nav-item-number">04/</span>
                    Partners</a>
                <a href="{{ route('contact.index') }}" data-text="05/ Contact" data-service="partneres"><span
                        class="text-[#00D7C7] nav-item-number">05/</span>
                    Contact</a>
            </div>
            <div class="absolute right-0 items-center justify-center hidden h-screen md:flex">
                <div id="case-studies" class="hidden floating">
                    <img src="{{ url('assets/images/sample.png') }}" alt="">
                </div>
            </div>
        </div>
    </div>

</nav>
