<section class="px-5 py-20 mt-10 md:px-0 md:mt-20">

    <div
        class="container flex flex-col-reverse items-center justify-between max-w-6xl mx-auto space-y-0 md:flex-row md:space-y-0">

        <div class="flex flex-col w-1/2 justify-center md:justify-start space-y-6 md:space-y-[30px]">
            <div data-aos="fade-up" data-aos-offset="200" data-aos-delay="50" data-aos-duration="700"
                data-aos-easing="ease-in-out"
                class="flex flex-row items-center justify-center space-x-5 md:justify-start">
                <p class="text-[#14233F] max-w-2xl text-base font-normal text-center md:text-[20px] md:text-left">
                    Feature Case Study
                </p>
                <div class="bg-[#00D7C7] h-0.5 w-20"></div>
            </div>

            <h1 data-aos="fade-up" data-aos-offset="200" data-aos-delay="50" data-aos-duration="600"
                data-aos-easing="ease-in-out"
                class="text-[#14233F] italic uppercase max-w-2xl text-4xl text-center font-black md:text-[80px] md:text-left leading-[30px] md:leading-[75px]">
                {{ $latest['title'][0] }}
            </h1>
            <div data-aos="fade-up" data-aos-offset="200" data-aos-delay="20" data-aos-duration="650"
                data-aos-easing="ease-in-out" class="flex items-center justify-center space-x-5 md:justify-start">
                <img src="{{ url('assets/content/button-arrow.svg') }}" alt="" class="w-8 md:w-auto" />
                <a href="{{ env('NOTION_PUBLIC_URI') . $latest['id'][0] }}"
                    class="text-[#14233F] hover:text-[#00D7C7] animate-bounce text-base md:text-[20px]">View
                    Case Study</a>
            </div>


        </div>
        <div class="w-1/2 md:absolute md:flex md:justify-center md:items-center md:right-0" data-aos="zoom-in-up"
            data-aos-offset="200" data-aos-delay="50" data-aos-duration="1000" data-aos-easing="ease-in-out">
            <img src="{{ $latest['image'][0] }}" alt="" class="py-10" />
        </div>
    </div>

</section>
