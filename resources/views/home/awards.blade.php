<section class="px-5 md:px-0 py-10">
    <p data-aos="fade-up" data-aos-offset="200" data-aos-delay="20" data-aos-duration="500" data-aos-easing="ease-in-out"
        class="text-center text-white text-3xl md:text-[40px] font-normal mb-16">Our Awards</p>
    <div class="container max-w-6xl grid grid-cols-3 md:grid-cols-5 gap-8 mx-auto">
        <div data-aos="fade-right" data-aos-offset="200" data-aos-delay="20" data-aos-duration="600"
            data-aos-easing="ease-in-out" class="card">
            <img src="{{ url('assets/awards/uk-dev-awards-2022-finalist.png') }}" alt="" />
        </div>
        <div data-aos="fade-right" data-aos-offset="200" data-aos-delay="20" data-aos-duration="650"
            data-aos-easing="ease-in-out" class="card">
            <img src="{{ url('assets/awards/uk-dev-awards-2022-winner.png') }}" alt="" />
        </div>
        <div data-aos="fade-right" data-aos-offset="200" data-aos-delay="20" data-aos-duration="700"
            data-aos-easing="ease-in-out" class="card">
            <img src="{{ url('assets/awards/eu-ecommerce-award-2022.png') }}" alt="" />
        </div>
        <div data-aos="fade-right" data-aos-offset="200" data-aos-delay="20" data-aos-duration="750"
            data-aos-easing="ease-in-out" class="card">
            <img src="{{ url('assets/awards/uk-ecommerce-award-2021.png') }}" alt="" />
        </div>
        <div data-aos="fade-right" data-aos-offset="200" data-aos-delay="20" data-aos-duration="800"
            data-aos-easing="ease-in-out" class="card">
            <img src="{{ url('assets/awards/uk-agency-award-2021.png') }}" alt="" />
        </div>

    </div>

</section>
