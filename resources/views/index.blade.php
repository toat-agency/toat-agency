<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    @if ($type == 'home')
        <title>Home - TOAT Agency</title>
    @elseif ($type == 'services')
        <title>Services - TOAT Agency</title>
    @elseif ($type == 'retainer')
        <title>Retainer - TOAT Agency</title>
    @elseif ($type == 'case-studies')
        <title>Retainer - TOAT Agency</title>
    @elseif ($type == 'partner')
        <title>Partner - TOAT Agency</title>
    @elseif ($type == 'contact')
        <title>Contact - TOAT Agency</title>
    @endif


    <!-- Fonts -->
    <style>
        @import url('https://fonts.cdnfonts.com/css/lota-grotesque-alt-1');
    </style>

    {{-- FontAwesome Icons --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css">

    {{-- AOS --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.css" />

    <!-- Owl Carousel -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" />
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" />

    {{-- Tailwind Css --}}
    @vite('resources/css/app.css')
</head>

<body class="{{ $type == 'case-studies' || $type == 'partner' || $type == 'contact' ? 'bg-white' : 'bg-[#0a162c] ' }}">
    {{-- Navigation --}}
    @if ($type == 'case-studies' || $type == 'partner' || $type == 'contact')
        @include('master.white_navigation')
    @else
        @include('master.navigation')
    @endif


    {{-- Home --}}
    @if ($type == 'home')
        @include('home.hero')
        @include('home.awards')
        @include('home.content')
        @include('home.works', $data)
        @include('home.client')
    @endif

    {{-- Services --}}
    @if ($type == 'services')
        @include('services.hero')
        @include('services.clients')
        @include('services.services')
    @endif

    {{-- Retainer --}}
    @if ($type == 'retainer')
        @include('retainer.hero')
        @include('retainer.services')
        @include('retainer.feature')
        @include('retainer.faqs')
    @endif

    {{-- Case Studies --}}
    @if ($type == 'case-studies')
        @include('case_study.hero', $latest)
        @include('case_study.content', $data)
    @endif

    @if ($type == 'partner')
        @include('partner.hero')
        @include('partner.partner')
        @include('partner.content')
    @endif

    {{-- CTA --}}
    @include('master.cta')

    {{-- Footer --}}
    @include('master.footer')
    {{-- Cursor Circle --}}
    <div id="cursor" class="hidden md:flex"></div>
    {{-- Jquery --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
    {{-- AOS --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.js"></script>
    {{-- Owl Carousel --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    {{-- Alpine JS --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/alpinejs/2.3.0/alpine-ie11.min.js"></script>
    {{-- Main JS --}}
    @if ($type == 'case-studies' || $type == 'partner' || ($type = 'contact'))
        <script src="{{ URL::asset('js/main-white.js') }}"></script>
    @else
        <script src="{{ URL::asset('js/main.js') }}"></script>
    @endif
</body>

</html>
