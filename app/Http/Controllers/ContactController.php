<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use FiveamCode\LaravelNotionApi\Entities\Page;

class ContactController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //type of page to show in index
        $type = 'contact';

        return view('index', compact('type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'company' => 'required',
            'project' => 'required',
            'budget' => 'required|numeric',
            'start_date' => 'required',
            'end_date' => 'required',
        ]);

        $page = new Page();

        # property: select
        $page->setSelect("Status", "New");

        # property: title
        $page->setTitle("Name", $request->name);

        # property: email
        $page->setEmail("Email", $request->email);

        # property: text
        $page->setText("Company", $request->company);
        $page->setText("Tell us about your project", $request->project);

        # property: number
        $page->setNumber("Budget", $request->budget);

        # property: date
        $page->setDate("Start Date", Carbon::parse($request->start_date));
        $page->setDate("End Date", Carbon::parse($request->end_date));

        // Create page and insert to database
        \Notion::pages()->createInDatabase(env('DATABASE_ID'), $page);

        return redirect()->route('contact.index')->with(['success' => 'Thank you for contacting us. We will contact you shortly.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
