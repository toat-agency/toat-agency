<section class="px-5 md:px-0 py-20 mt-10 md:mt-20">

    <div
        class="container max-w-6xl flex flex-col-reverse md:flex-row items-center justify-between mx-auto space-y-0 md:space-y-0">

        <div class="flex flex-col space-y-6 md:space-y-[30px]">
            <div data-aos="fade-up" data-aos-offset="200" data-aos-delay="20" data-aos-duration="650"
                data-aos-easing="ease-in-out" class="flex items-center space-x-5 justify-center md:justify-start">
                <img src="{{ url('assets/images/button-services.svg') }}" alt="" class="w-8 md:w-auto" />
                <h1 data-aos="fade-up" data-aos-offset="200" data-aos-delay="50" data-aos-duration="600"
                    data-aos-easing="ease-in-out"
                    class="text-white uppercase max-w-3xl text-4xl text-center font-black md:text-[50px] md:text-left leading-[35px] md:leading-[45px]">
                    Services
                </h1>
            </div>

            <h1 data-aos="fade-up" data-aos-offset="200" data-aos-delay="50" data-aos-duration="600"
                data-aos-easing="ease-in-out"
                class="text-white max-w-3xl text-2xl text-center font-normal md:text-[35px] md:text-left leading-[35px] md:leading-[45px]">
                <span class="font-bold text-[#00D7C7]">Lorem Ipsum </span>is simply dummy text of the printing and
                typesetting industry.
            </h1>
            <p data-aos="fade-up" data-aos-offset="200" data-aos-delay="50" data-aos-duration="700"
                data-aos-easing="ease-in-out"
                class="text-[#9A9A9A] max-w-2xl text-base font-normal text-center md:text-[20px] md:text-left leading-[25px]">
                We’re a growth-focused <span class="font-bold text-[#00D7C7]">Laravel Development</span> agency,
                partnering with ambitious teams, to help grow and
                scale their digital presence by developing tools, systems + websites.
            </p>
        </div>
        <div class="md:w-1/2 p-6" data-aos="zoom-in-up" data-aos-offset="200" data-aos-delay="50"
            data-aos-duration="1000" data-aos-easing="ease-in-out">
            <img src="{{ url('assets/images/hero-image-services.png') }}" alt="" />
        </div>
    </div>
    <div data-aos="fade-right" data-aos-offset="200" data-aos-delay="50" data-aos-duration="800"
        data-aos-easing="ease-in-out" class="md:w-1/2 mx-auto py-10">
        <img src="{{ url('assets/images/lines.svg') }}" alt="" />
    </div>

</section>
