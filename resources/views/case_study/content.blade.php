<section class="px-5 md:px-0 py-10 md:mt-20 ">

    <div class="container grid grid-cols-1 md:grid-cols-2 gap-10 max-w-6xl mx-auto">
        @foreach ($data as $index => $item)
            @if ($index % 2 == 0)
                <div data-aos="fade-up" data-aos-offset="200" data-aos-delay="20" data-aos-duration="600"
                    data-aos-easing="ease-in-out" class="flex flex-col space-y-5">
                @else
                    <div data-aos="fade-up" data-aos-offset="200" data-aos-delay="20" data-aos-duration="700"
                        data-aos-easing="ease-in-out" class="flex flex-col space-y-5 md:mt-20">
            @endif

            <a href="{{ env('NOTION_PUBLIC_URI') . $item['id'][0] }}">
                <div class="img-overlay relative">
                    <div class="w-full md:p-0">
                        <img src="{{ $item['image'][0] }}" alt="" />
                    </div>
                    <div class="overlay p-10 absolute top-0 bottom-0 right-0 left-0 opacity-0">
                        <div class="flex flex-col justify-between h-full">
                            <div class="text-base font-normal text-center md:text-lg md:text-left text-[#00D7C7]">
                                @foreach ($item['category'] as $category)
                                    <p>{{ $category }}</p>
                                @endforeach

                            </div>
                            <div class="space-y-2">
                                <p
                                    class="text-white italic uppercase text-4xl font-black text-center md:text-[30px] md:text-left">
                                    {{ $item['title'][0] }}</p>
                                <p
                                    class="text-base font-normal text-center md:text-lg md:text-left text-white line-clamp-2">
                                    {{ $item['description'][0] }}
                                </p>
                            </div>
                        </div>
                    </div>

                </div>
            </a>
            <div class="flex flex-row items-center space-x-8">
                <p class="text-[#14233F] max-w-sm text-base font-normal  md:text-[18px] text-left">
                    {{ $item['title'][0] }}
                </p>
                <div class="h-0.5 w-full bg-[#14233F]"></div>
            </div>
    </div>
    @endforeach

    </div>

</section>
