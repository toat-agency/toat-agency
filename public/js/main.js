$(document).ready(function () {
    $(".owl-carousel").owlCarousel();
});
$("[data-service]")
    .mouseover(function () {
        const theValue = $(this).attr("data-service");

        $("#" + theValue)
            .removeClass("hidden")
            .addClass("flex motion-reduce:animate-bounce");
        // alert(theValue);
    })
    .mouseleave(function () {
        const theValue = $(this).attr("data-service");
        $("#" + theValue)
            .addClass("hidden")
            .removeClass("flex");
    });

// carousel
var owl = $(".owl-carousel");
owl.owlCarousel({
    items: 3,
    loop: true,
    margin: 15,
    autoplay: true,
    autoplayTimeout: 3000,
    autoplayHoverPause: true,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
            loop: true,
        },
        640: {
            items: 2,
            loop: true,
        },
        768: {
            items: 3,
            loop: true,
        },
        1024: {
            items: 3,
            loop: true,
        },
    },
});
$(".play").on("click", function () {
    owl.trigger("play.owl.autoplay", [1000]);
});
$(".stop").on("click", function () {
    owl.trigger("stop.owl.autoplay");
});

// hamburger
const btn = document.getElementById("menu-btn");
const nav = document.getElementById("navmenu");

btn.addEventListener("click", () => {
    btn.classList.toggle("open");
    nav.classList.toggle("left-[-110%]");
    nav.classList.toggle("left-0");
});

// AOS
AOS.init();

// Cursor
let cursor = document.getElementById("cursor");

document.addEventListener("mousemove", (e) => {
    // console.log(e);
    cursor.style.left = `${e.clientX - 15}px`;
    cursor.style.top = `${e.clientY - 10}px`;
});
