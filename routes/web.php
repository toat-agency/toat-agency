<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\PartnerController;
use App\Http\Controllers\RetainerController;
use App\Http\Controllers\ServicesController;
use App\Http\Controllers\CaseStudyController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('/', HomeController::class);
Route::resource('services', ServicesController::class);
Route::resource('retainer', RetainerController::class);
Route::resource('case-studies', CaseStudyController::class);
Route::resource('partner', PartnerController::class);
Route::resource('contact', ContactController::class);
