<section class="px-5 pb-20 md:px-0">

    <div class="container grid items-center max-w-6xl grid-cols-4 mx-auto text-center gap-x-12 gap-y-20">
        <i data-aos="fade-up" data-aos-offset="200" data-aos-delay="50" data-aos-duration="600" data-aos-easing="ease-in-out"
            class="text-gray-400 text-8xl fa-brands fa-wordpress"></i>
        <i data-aos="fade-up" data-aos-offset="200" data-aos-delay="50" data-aos-duration="700"
            data-aos-easing="ease-in-out" class="text-gray-400 text-8xl fa-brands fa-facebook"></i>
        <i data-aos="fade-up" data-aos-offset="200" data-aos-delay="50" data-aos-duration="800"
            data-aos-easing="ease-in-out" class="text-gray-400 text-8xl fa-brands fa-linkedin"></i>
        <i data-aos="fade-up" data-aos-offset="200" data-aos-delay="50" data-aos-duration="900"
            data-aos-easing="ease-in-out" class="text-gray-400 text-8xl fa-brands fa-docker"></i>
        <i data-aos="fade-up" data-aos-offset="200" data-aos-delay="50" data-aos-duration="1000"
            data-aos-easing="ease-in-out" class="text-gray-400 text-8xl fa-brands fa-medium"></i>
        <i data-aos="fade-up" data-aos-offset="200" data-aos-delay="50" data-aos-duration="1100"
            data-aos-easing="ease-in-out" class="text-gray-400 text-8xl fa-brands fa-product-hunt"></i>
        <i data-aos="fade-up" data-aos-offset="200" data-aos-delay="50" data-aos-duration="1200"
            data-aos-easing="ease-in-out" class="text-gray-400 text-8xl fa-brands fa-dashcube"></i>
        <i data-aos="fade-up" data-aos-offset="200" data-aos-delay="50" data-aos-duration="1300"
            data-aos-easing="ease-in-out" class="text-gray-400 text-8xl fa-brands fa-readme"></i>
    </div>
</section>
