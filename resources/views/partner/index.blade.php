<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Partner - TOAT Agency</title>

    <!-- Fonts -->
    <style>
        @import url('https://fonts.cdnfonts.com/css/lota-grotesque-alt-1');
    </style>

    {{-- FontAwesome Icons --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css">

    {{-- AOS --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.css" />

    <!-- Owl Carousel -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" />
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" />

    {{-- Tailwind Css --}}
    @vite('resources/css/app.css')
</head>

<body>
    {{-- Navigation --}}
    @include('master.body.white_navigation', $total)
    {{-- Hero --}}
    @include('master.partner.hero');
    {{-- Partner --}}
    @include('master.partner.partner');
    {{-- Content --}}
    @include('master.partner.content');
    {{-- CTA --}}
    @include('master.body.cta')

    {{-- Footer --}}
    @include('master.body.footer')
    {{-- Cursor Circle --}}
    <div id="cursor" class="hidden md:flex"></div>
    <script src="{{ URL::asset('js/cursor.js') }}"></script>
    {{-- Jquery --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
    {{-- AOS --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.js"></script>
    {{-- Owl Carousel --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script src="{{ URL::asset('js/main-white.js') }}"></script>
</body>

</html>
