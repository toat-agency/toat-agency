<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends BaseController
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // Show home with case study data from notion api

        $caseStudy = json_decode(\Notion::block(env('CASE_STUDY_ID'))->children()->asJson());

        $id = array();
        $image = array();
        $description = array();
        $category = array();
        $title = array();
        $data = array();

        foreach ($caseStudy as $items) {

            if ($items->type == 'image') {

                array_push($image, $items->content);
            }
            if ($items->type == 'paragraph') {

                array_push($description, $items->text);
            }
            if ($items->type == 'bulleted_list_item') {

                array_push($category, $items->text);
            }
            if ($items->type == 'child_page') {

                $newString = str_replace('-', '', $items->id);
                array_push($id, $newString);
                array_push($title, $items->content);

                array_push(
                    $data,
                    [
                        'image' => $image,
                        'description' => $description,
                        'category' => $category,
                        'title' => $title,
                        'id' => $id
                    ]
                );
                $id = array();
                $image = array();
                $description = array();
                $category = array();
                $title = array();
            }
        }
        //type of page to show in index
        $type = 'home';

        return view('index', compact('data', 'type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
