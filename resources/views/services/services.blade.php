<section class="px-5 py-10 mt-20 md:px-0">
    <p data-aos="fade-up" data-aos-offset="200" data-aos-delay="20" data-aos-duration="500" data-aos-easing="ease-in-out"
        class="text-center text-white text-3xl md:text-[40px] font-normal mb-16">Clients Using Our Services</p>
    <div class="container grid max-w-6xl grid-cols-1 gap-12 mx-auto md:grid-cols-3 gap-y-20">
        @foreach ($servicesData as $index => $item)
            <div data-aos="fade-up" data-aos-offset="200" data-aos-delay="20" data-aos-duration="600"
                data-aos-easing="ease-in-out" class="flex flex-row items-start space-x-5">
                <i class="{{ $item['icon'][0] }}"></i>
                <div class="flex flex-col space-y-[30px]">
                    <p
                        class="italic uppercase text-base font-black  md:text-[28px] text-left text-white leading-[25px] md:leading-[35px]">
                        {{ $item['title'][0] }}</p>
                    <p class="text-base font-light  md:text-[15px] text-left text-white">
                        {{ $item['description'][0] }}</p>
                    <div class="flex items-center justify-center space-x-5 md:justify-start">
                        <img src="{{ url('assets/content/button-arrow.svg') }}" alt="" class="w-8 md:w-12" />
                        <a href="{{ env('NOTION_PUBLIC_URI') . $item['id'][0] }}"
                            class="text-white font-light hover:text-[#00D7C7] animate-bounce text-base md:text-[18px]">Check
                            Services</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

</section>
