<section class="px-5 py-40 md:px-0">
    <div class="container flex flex-col items-center max-w-6xl mx-auto space-x-0 space-y-12">
        <div class="grid grid-cols-2 gap-12">
            <div class="flex flex-row items-center space-x-5">
                <img src="{{ asset('assets/content/button-arrow.svg') }}" alt="">
                <p data-aos="fade-right" data-aos-offset="200" data-aos-delay="20" data-aos-duration="600"
                    data-aos-easing="ease-in-out" class="text-4xl italic font-bold text-white uppercase">LAUNCH THE BEST
                    FEATURES
                </p>
            </div>
            <p data-aos="fade-right" data-aos-offset="200" data-aos-delay="20" data-aos-duration="700"
                data-aos-easing="ease-in-out" class="text-xl text-[#9a9a9a]">Lorem ipsum dolor sit amet, consectetur
                adipiscing elit, sed do eiusmod
                tempor
                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                velit esse cillum dolore eu fugiat nulla pariatur.</p>
        </div>
        <div class="grid grid-cols-2 gap-12">
            <div class="flex flex-row items-center space-x-5">
                <img src="{{ asset('assets/content/button-arrow.svg') }}" alt="">
                <p data-aos="fade-right" data-aos-offset="200" data-aos-delay="20" data-aos-duration="800"
                    data-aos-easing="ease-in-out" class="text-4xl italic font-bold text-white uppercase">CONVERT MORE
                    CUSTOMERS</p>
            </div>
            <p data-aos="fade-right" data-aos-offset="200" data-aos-delay="20" data-aos-duration="900"
                data-aos-easing="ease-in-out" class="text-xl text-[#9a9a9a]">Lorem ipsum dolor sit amet, consectetur
                adipiscing elit, sed do eiusmod
                tempor
                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                velit esse cillum dolore eu fugiat nulla pariatur.</p>
        </div>
        <div class="grid grid-cols-2 gap-12">
            <div class="flex flex-row items-center space-x-5">
                <img src="{{ asset('assets/content/button-arrow.svg') }}" alt="">
                <p data-aos="fade-right" data-aos-offset="200" data-aos-delay="20" data-aos-duration="1000"
                    data-aos-easing="ease-in-out" class="text-4xl italic font-bold text-white uppercase">ENHANCE YOUR
                    BRAND POSITIONING</p>
            </div>
            <p data-aos="fade-right" data-aos-offset="200" data-aos-delay="20" data-aos-duration="1300"
                data-aos-easing="ease-in-out" class="text-xl text-[#9a9a9a]">Lorem ipsum dolor sit amet, consectetur
                adipiscing elit, sed do eiusmod
                tempor
                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                velit esse cillum dolore eu fugiat nulla pariatur.</p>
        </div>
    </div>
</section>
