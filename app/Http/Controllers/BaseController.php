<?php

namespace App\Http\Controllers;

use Notion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class BaseController extends Controller
{

    public function __construct()
    {
        // Share the case study total number to navigation view
        $caseStudy = json_decode(Notion::block(env('CASE_STUDY_ID'))->children()->asJson());

        $title = array();
        $total = array();

        foreach ($caseStudy as $items) {

            if ($items->type == 'child_page') {
                array_push($title, $items->content);

                array_push(
                    $total,
                    [
                        'title' => $title,
                    ]
                );
                $title = array();
            }
        }

        View::share('total', $total);
    }
}
