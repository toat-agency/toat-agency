<section class="px-5 pt-40 pb-40 md:px-0">

    <div class="container relative flex flex-col items-center justify-between max-w-6xl mx-auto space-y-8 text-center">

        <p data-aos="fade-up" data-aos-offset="200" data-aos-delay="50" data-aos-duration="900" data-aos-easing="ease-in-out"
            class="text-2xl italic text-[#0A162C] uppercase">NEARLY A DECADE OF
            BUILDING TRUST
        </p>
        <p data-aos="fade-up" data-aos-offset="200" data-aos-delay="50" data-aos-duration="1000"
            data-aos-easing="ease-in-out" class="text-[#0A162C] text-4xl font-bold">Our teams have worked with almost
            <span span class="highlight"> 50+
                corporations </span> and <span span class="highlight">start-up businesses</span>
            since 2017.
        </p>
    </div>
    <div data-aos="fade-up" data-aos-offset="200" data-aos-delay="50" data-aos-duration="800"
        data-aos-easing="ease-in-out" class="py-10 mx-auto mt-20 md:w-1/2">
        <img src="{{ url('assets/images/lines_dark.svg') }}" alt="" />
    </div>
</section>
